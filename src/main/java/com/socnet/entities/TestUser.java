package com.socnet.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TestUser {

    private long id;
    @JsonProperty
    private String login;

    @JsonProperty
    private String password;

    private String email;

    public TestUser() {
    }

    public TestUser(long id, String login, String password, String email) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
