package com.socnet.entities;


import com.fasterxml.jackson.annotation.JsonProperty;

public class requestUser {

    @JsonProperty
    private String login;

    @JsonProperty
    private String password;

    private String email;

    private String name;

    public requestUser() {
    }


    public requestUser(String login, String password, String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public requestUser(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "requestUser [" +
                "login=" + login +
                ", password=" + password +
                ", email=" + email +
                ']';
    }
}
