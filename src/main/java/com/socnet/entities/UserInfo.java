package com.socnet.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "userinfos")
public class UserInfo {

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "status")
    private String status;

    @Transient
    private Boolean block1;

    @Column(name = "dob")
    private String dob;

    @Column(name = "htown")
    private String htown;

    @Transient
    private Boolean block2;

    @Column(name = "instagram")
    private String instagram;

    @Column(name = "vk")
    private String vk;

    @Column(name = "url1")
    private String url1;

    @Column(name = "urlname1")
    private String urlname1;

    @Column(name = "url2")
    private String url2;

    @Column(name = "urlname2")
    private String urlname2;

    @Transient
    private Boolean block3;

    @Column(name = "music")
    private String music;

    @Column(name = "about")
    private String about;

    @JsonIgnore
    @OneToOne
    @MapsId
    private User user;

    public UserInfo() {
    }

    public UserInfo(String name, User user) {
        this.name = name;
        this.user = user;
        this.status = "";
        this.dob = "";
        this.htown = "";
        this.instagram = "";
        this.vk = "";
        this.url1 = "";
        this.urlname1 = "";
        this.url2 = "";
        this.urlname2 = "";
        this.music = "";
        this.about = "";
    }

    public UserInfo(String name, String status, String dob, String htown,
                    String instagram, String vk, String url1, String urlname1,
                    String url2, String urlname2, String music, String about) {
        this.name = name;
        this.status = status;
        this.dob = dob;
        this.htown = htown;
        this.instagram = instagram;
        this.vk = vk;
        this.url1 = url1;
        this.urlname1 = urlname1;
        this.url2 = url2;
        this.urlname2 = urlname2;
        this.music = music;
        this.about = about;
        this.block1=dob.isEmpty() && htown.isEmpty();
        this.block2=instagram.isEmpty() && vk.isEmpty() && url1.isEmpty() && url2.isEmpty();
        this.block3=music.isEmpty() && about.isEmpty();
    }

    public UserInfo(long id, String name, String status, String dob, String htown,
                    String instagram, String vk, String url1, String urlname1,
                    String url2, String urlname2, String music, String about) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.dob = dob;
        this.htown = htown;
        this.instagram = instagram;
        this.vk = vk;
        this.url1 = url1;
        this.urlname1 = urlname1;
        this.url2 = url2;
        this.urlname2 = urlname2;
        this.music = music;
        this.about = about;
        this.block1=dob.isEmpty() && htown.isEmpty();
        this.block2=instagram.isEmpty() && vk.isEmpty() && url1.isEmpty() && url2.isEmpty();
        this.block3=music.isEmpty() && about.isEmpty();
    }

    public void setBlocks(){
        this.setBlock1(!(dob.isEmpty() && htown.isEmpty()));
        this.setBlock2(!(instagram.isEmpty() && vk.isEmpty() && url1.isEmpty() && url2.isEmpty()));
        this.setBlock3(!(music.isEmpty() && about.isEmpty()));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getBlock1() {
        return block1;
    }

    public void setBlock1(Boolean block1) {
        this.block1 = block1;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getHtown() {
        return htown;
    }

    public void setHtown(String htown) {
        this.htown = htown;
    }

    public Boolean getBlock2() {
        return block2;
    }

    public void setBlock2(Boolean block2) {
        this.block2 = block2;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getVk() {
        return vk;
    }

    public void setVk(String vk) {
        this.vk = vk;
    }

    public String getUrl1() {
        return url1;
    }

    public void setUrl1(String url1) {
        this.url1 = url1;
    }

    public String getUrlname1() {
        return urlname1;
    }

    public void setUrlname1(String urlname1) {
        this.urlname1 = urlname1;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrlname2() {
        return urlname2;
    }

    public void setUrlname2(String urlname2) {
        this.urlname2 = urlname2;
    }

    public Boolean getBlock3() {
        return block3;
    }

    public void setBlock3(Boolean block3) {
        this.block3 = block3;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
