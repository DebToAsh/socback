package com.socnet.validators;

import com.socnet.config.AppConfig;
import com.socnet.entities.User;
import com.socnet.entities.requestUser;
//import com.socnet.dao.UserDAO;
import com.socnet.service.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.persistence.NoResultException;

public class LoginValidator {

    private UserService userService;

    public LoginValidator(UserService userService) {
        this.userService = userService;
    }

    public String validate(requestUser user) {
        User locUser = this.loginTaken(user.getLogin());
        if (locUser != null) {
            if (locUser.getPassword().equals(user.getPassword())) return "OK";
        }
        return "Wrong login or password!";
    }

    public User loginTaken(String value) {
//        AnnotationConfigApplicationContext context= new AnnotationConfigApplicationContext(AppConfig.class);
//        UserService service = context.getBean(UserService.class);
        User user;
        try {
            user = userService.findBy("login", value);
        } catch (NoResultException e) {
            user = null;
        }
//        context.close();
        return user;
    }
}
