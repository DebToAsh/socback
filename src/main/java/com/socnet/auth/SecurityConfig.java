package com.socnet.auth;

import com.socnet.entities.UserRoleEnum;
import com.socnet.service.UserDetailsServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static String REALM = "MY_TEST_REALM";

//    @Autowired
//    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(getUserDetailsService()).passwordEncoder(encoder());
    }

    @Bean
    public RestAuthenticationEntryPoint getBasicAuthEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService getUserDetailsService() {
        return new UserDetailsServiceImp();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()//.anyRequest().authenticated()
                .antMatchers("/login/user").authenticated()
                .antMatchers("/login/username").permitAll()
                .antMatchers("/signup/add").permitAll()
                .antMatchers("/userinfo1/**").authenticated()
                .antMatchers("/userinfo/**").permitAll()
                .antMatchers("/admin/**").hasRole(UserRoleEnum.ADMIN.name())
                .antMatchers("/app/getFriendList").permitAll()
                .antMatchers("/app/profile/**").permitAll()
//                .and()
//                .authorizeRequests().anyRequest().permitAll()
                .and()
                .httpBasic().realmName(REALM).authenticationEntryPoint(getBasicAuthEntryPoint())
                .and()
                .logout().logoutUrl("/logout").invalidateHttpSession(true).deleteCookies("JSESSIONID")
                .logoutSuccessHandler(new CustomLogoutSuccessHandler());//.addLogoutHandler(new CustomLogoutHandler());

//        http
//                .csrf().disable()
//                .exceptionHandling()
////                .authenticationEntryPoint(restAuthenticationEntryPoint())
//                .and()
//                .authorizeRequests()
//                .antMatchers("/login/add").permitAll()
//                .antMatchers("/login/user").permitAll()
//                .antMatchers("/app/**").authenticated()
//                .antMatchers("/login/getList").hasRole("ADMIN")
//                .antMatchers("/app/admin/**").hasRole("ADMIN")
//                .and()
////                .formLogin()
////                .successHandler(mySuccessHandler)
////                .failureHandler(myFailureHandler)
////                .and()
//                .logout();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
    }
}
