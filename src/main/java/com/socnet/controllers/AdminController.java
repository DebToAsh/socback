package com.socnet.controllers;

import com.socnet.entities.CustomResponse;
import com.socnet.entities.User;
import com.socnet.entities.UserRoleEnum;
import com.socnet.entities.requestUser;
import com.socnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/admin")
public class AdminController {

    private UserService userService;


    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/getList", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsers() {
//        AnnotationConfigApplicationContext context= new AnnotationConfigApplicationContext(AppConfig.class);
//        UserService service = context.getBean(UserService.class);
        List<User> users = userService.getAll();
//        users.add(new User(1, "debt", "12345", "debt@debt.com", UserRoleEnum.ADMIN));
//        users.add(new User(2, "debt1", "12345", "debt@debt.com",UserRoleEnum.ADMIN));
//        users.add(new User(3, "debt2", "12345", "debt@debt.com",UserRoleEnum.USER));
//        users.add(new User(4, "debt3", "12345", "debt@debt.com",UserRoleEnum.ADMIN));
//        context.close();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/swap", method = RequestMethod.POST)
    public ResponseEntity<CustomResponse<String>> setAdmin(@RequestBody requestUser ruser) {
        try {
            User user = userService.findByLogin(ruser.getLogin());
            switch (user.getRole()) {
                case ADMIN:
                    user.setRole(UserRoleEnum.USER);
                    break;
                case USER:
                    user.setRole(UserRoleEnum.ADMIN);
                    break;
                default:
                    return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"Roles error"}, 2), HttpStatus.OK);
            }
            userService.update(user);
            return new ResponseEntity<>(new CustomResponse<>(null, null, 1), HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"No such user"}, 3), HttpStatus.OK);
        }
    }
}
