package com.socnet.controllers;


import com.socnet.entities.*;
import com.socnet.service.UserInfoService;
import com.socnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PersistenceException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
//@RequestMapping(path="/app")
public class UserController {

    private UserService userService;

    private UserInfoService userInfoService;


    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired(required = true)
    @Qualifier(value = "userInfoService")
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @GetMapping("/userinfo1/{login}")
    public requestUser getFullUser(@PathVariable String login) {
        User user = userService.findBy("login", login);
        return new requestUser(user.getLogin(), user.getPassword(), user.getEmail());
    }

    @GetMapping("/userinfo/{login}")
    public ResponseEntity<TestUser> getUser(@PathVariable String login) {
        User user = userService.findBy("login", login);
        HttpHeaders headers = new HttpHeaders();
        if (user != null) {
            try {
                SecurityContext securityContext = SecurityContextHolder.getContext();
                AuthUser authUser;
                if (null != securityContext.getAuthentication()) {
                    authUser = (AuthUser) securityContext.getAuthentication().getPrincipal();
                    if (authUser.getUsername().equals(login)) {
                        headers.add("logged", "true");
                        return new ResponseEntity<>(new TestUser(authUser.getId(), user.getLogin(), user.getPassword(), user.getEmail()), headers, HttpStatus.OK);
                    }
                }
            } catch (NullPointerException ignore) {
            }
            headers.add("logged", "false");
            return new ResponseEntity<>(new TestUser(0, user.getLogin(), "", ""), headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
        }

    }

    @RequestMapping(value = "app/getFriendList", method = RequestMethod.GET)
    public ResponseEntity<CustomResponse<List<Friend>>> getFriends() {
//        AnnotationConfigApplicationContext context= new AnnotationConfigApplicationContext(AppConfig.class);
//        UserService service = context.getBean(UserService.class);
        List<User> users = userService.getAll();
        List<Friend> friends = new ArrayList<>();
        for (User user : users
        ) {
            friends.add(new Friend(user.getId(), user.getLogin()));
        }
//        users.add(new User(1, "debt", "12345", "debt@debt.com", UserRoleEnum.ADMIN));
//        users.add(new User(2, "debt1", "12345", "debt@debt.com",UserRoleEnum.ADMIN));
//        users.add(new User(3, "debt2", "12345", "debt@debt.com",UserRoleEnum.USER));
//        users.add(new User(4, "debt3", "12345", "debt@debt.com",UserRoleEnum.ADMIN));
//        context.close();
        return new ResponseEntity<>(new CustomResponse<>(friends, null, 1), HttpStatus.OK);
    }

    @GetMapping(value = "app/profile/{login}")
    public ResponseEntity<CustomResponse<UserInfo>> profileInfo(@PathVariable String login) {
        try {
            UserInfo userInfo;
            User user;
            user = userService.findBy("login", login);
            try {
                userInfo = user.getUserInfo();
                userInfo.setBlocks();
                return new ResponseEntity<>(new CustomResponse<>(userInfo, null, 1), HttpStatus.OK);
            } catch (NullPointerException e) {
                return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"No Info"}, 2), HttpStatus.OK);
            }
        } catch (NullPointerException e) {
            return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"No logged user"}, 3), HttpStatus.OK); //TODO: status 3 no user error, status 2 logged status.
        }
    }

    @GetMapping(value = "app/profile/edit")
    public ResponseEntity<CustomResponse<UserInfo>> editInfo() {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (null != securityContext.getAuthentication()) {
            AuthUser authUser = (AuthUser) securityContext.getAuthentication().getPrincipal();
            UserInfo userInfo;
            try {
                userInfo = userInfoService.get(authUser.getId());
            } catch (NullPointerException e) {
                return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"e"}, 2), HttpStatus.OK);
            }
            return new ResponseEntity<>(new CustomResponse<>(userInfo, null, 1), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"No logged user"}, 3), HttpStatus.OK);
        }

    }

    @PostMapping(value = "app/profile/add")
    public ResponseEntity<CustomResponse<String>> addInfo(@RequestBody UserInfo userInfo) {
        try {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            if (null != securityContext.getAuthentication()) {
                AuthUser authUser = (AuthUser) securityContext.getAuthentication().getPrincipal();
                UserInfo info = userInfo;
                User user = userService.findBy("login", authUser.getUsername());
                info.setUser(user);
                userInfoService.add(info);
                return new ResponseEntity<>(new CustomResponse<>("OK", null, 1), HttpStatus.OK);
            }
            return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"No logged user"}, 3), HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"Nullptr"}, 2), HttpStatus.OK);
        } catch (PersistenceException e) {
            return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"Persist"}, 2), HttpStatus.OK);
        }
    }

    @PostMapping(value = "app/profile/update")
    public ResponseEntity<CustomResponse<String>> updateInfo(@RequestBody UserInfo userInfo) {
        try {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            if (null != securityContext.getAuthentication()) {
                AuthUser authUser = (AuthUser) securityContext.getAuthentication().getPrincipal();
                UserInfo info = userInfo;
                info.setId(authUser.getId());
                userInfoService.update(info);
                return new ResponseEntity<>(new CustomResponse<>("OK", null, 1), HttpStatus.OK);
            }
            return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"No logged user"}, 3), HttpStatus.OK);
        } catch (Throwable e) {
            return new ResponseEntity<>(new CustomResponse<>(null, new String[]{"Connection error"}, 2), HttpStatus.OK);
        }
    }
}
