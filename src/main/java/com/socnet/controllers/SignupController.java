package com.socnet.controllers;

import com.socnet.entities.User;
import com.socnet.entities.UserInfo;
import com.socnet.entities.requestUser;
import com.socnet.service.UserInfoService;
import com.socnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/signup")
public class SignupController {

    private UserService userService;

    private UserInfoService userInfoService;

    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired(required = true)
    @Qualifier(value = "userInfoService")
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @PostMapping("/add")
    public ResponseEntity<String> add(@RequestBody requestUser rUser) {
        if (this.loginTaken(rUser.getLogin()))
            return new ResponseEntity<>("This login is already taken!", HttpStatus.UNAUTHORIZED);
        if (this.emailTaken(rUser.getEmail()))
            return new ResponseEntity<>("This E-mail is already taken!",HttpStatus.UNAUTHORIZED);
        User user = new User(rUser);
        UserInfo userInfo=new UserInfo(rUser.getName(),user);
        user.setUserInfo(userInfo);
        userService.add(user);
        return new ResponseEntity<>("OK",HttpStatus.OK);
    }


    private boolean loginTaken(String value) {
//        UserDAO service=new UserDAO();
        try {
            userService.findBy("login", value);
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private boolean emailTaken(String value) {
//        UserDAO service=new UserDAO();
        try {
            userService.findBy("email", value);
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

}
