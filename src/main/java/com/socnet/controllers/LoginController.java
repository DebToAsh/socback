package com.socnet.controllers;

import com.socnet.entities.*;
import com.socnet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

//import javax.ws.rs.*;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;

@RestController
@RequestMapping(path = "/login")
public class LoginController {

    private UserService userService;


    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @PostMapping("/user")
    public ResponseEntity<CustomResponse<String>> login(@RequestBody requestUser rUser) {
        String role = null;
        String[] errors = {"NANI"};
        try {
            User user = userService.findBy("login", rUser.getLogin());
            role = user.getRole().name();
        } catch (NoResultException e) {
//            System.out.println(login);
        }
        if (role != null)
            return new ResponseEntity<>(new CustomResponse<>(role, null, 1), HttpStatus.OK);
        else
            return new ResponseEntity<>(new CustomResponse<>(role, errors, 2), HttpStatus.OK);
//       public void login(HttpSession session)
//        LoginValidator service = new LoginValidator(this.userService);
//        return service.validate(user);
//        session.
    }

    @GetMapping("/username")
    public String getUsername(HttpServletRequest request) throws NullPointerException {
//        User user = new User("debt", "12345", UserRoleEnum.ADMIN.name(), UserRoleEnum.ADMIN);
        String username = (String) request.getSession().getAttribute("login");
        return username;
    }

//    @PutMapping("/add")
//    public String add(@RequestBody User user) {
//        try {
//            userService.add(user);
//        } catch (Exception e) {
//            return "smth gone wrong";
//        }
//        return user.toString() + " added successfully";
//    }





}
