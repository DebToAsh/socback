package com.socnet.config;


import com.socnet.dao.UserDAOImp;
import com.socnet.dao.UserInfoDAOImp;
import com.socnet.entities.User;
import com.socnet.entities.UserInfo;
import com.socnet.service.UserInfoServiceImp;
import com.socnet.service.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:db.properties")
@EnableTransactionManagement
public class DatabaseConfig {

    @Autowired
    private Environment env;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("db.driver"));
        dataSource.setUrl(env.getProperty("db.url"));
        dataSource.setUsername(env.getProperty("db.username"));
        dataSource.setPassword(env.getProperty("db.password"));
        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setDataSource(dataSource());

        Properties props = new Properties();
        props.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        props.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));

        factoryBean.setHibernateProperties(props);
        factoryBean.setAnnotatedClasses(User.class, UserInfo.class);
        return factoryBean;
    }

    @Bean
    public HibernateTransactionManager getTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    @Bean
    public UserDAOImp userDAO(){
        UserDAOImp userDAOImp=new UserDAOImp();
        userDAOImp.setSessionFactory(sessionFactory().getObject());
        return userDAOImp;
    }

    @Bean
    public UserServiceImp userService(){
        UserServiceImp userServiceImp=new UserServiceImp();
        userServiceImp.setUserDAO(userDAO());
        return userServiceImp;
    }

    @Bean
    public UserInfoDAOImp userInfoDAO(){
        UserInfoDAOImp userInfoDAOImp=new UserInfoDAOImp();
        userInfoDAOImp.setSessionFactory(sessionFactory().getObject());
        return userInfoDAOImp;
    }

    @Bean
    public UserInfoServiceImp userInfoService(){
        UserInfoServiceImp userInfoServiceImp=new UserInfoServiceImp();
        userInfoServiceImp.setUserInfoDAO(userInfoDAO());
        return userInfoServiceImp;
    }
}