package com.socnet.dao;

import com.socnet.entities.User;

import java.util.List;

public interface UserDAO {
    void add(User user);
    long addNew(User user);
    List<User> getAll();
    User findBy(String what, String param);
    void update(User user);
}