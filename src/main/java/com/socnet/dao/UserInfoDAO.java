package com.socnet.dao;

import com.socnet.entities.UserInfo;

public interface UserInfoDAO {
    void add(UserInfo userInfo);
    UserInfo get(long id);
    void update(UserInfo userInfo);
}
