package com.socnet.dao;

import com.socnet.entities.User;
import com.socnet.entities.UserInfo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserInfoDAOImp implements UserInfoDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(UserInfo userInfo) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(userInfo);
//        em.getTransaction().begin();
//        User userFromDB = em.merge(user);
//        em.getTransaction().commit();
//        return userFromDB;
    }

    public UserInfo get(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.get(UserInfo.class,id);
//        return em.find(User.class, id);
    }

    //    public void delete(long id) {
//        em.getTransaction().begin();
//        em.remove(getId(id));
//        em.getTransaction().commit();
//    }
//
//
//    @Override
//    public User findBy(String what, String param) {
//        Session session = this.sessionFactory.getCurrentSession();
//        CriteriaBuilder builder=session.getCriteriaBuilder();
//        CriteriaQuery<User> query = builder.createQuery(User.class);
//        Root<User> root = query.from(User.class);
//        query.select(root).where(builder.equal(root.get(what),param));
//        Query<User> q = session.createQuery(query);
//        return q.getSingleResult();
////        CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
////        CriteriaQuery<User> cq = cb.createQuery(User.class);
////        Root<User> root = cq.from(User.class);
////        cq.select(root);
////        cq.where(cb.equal(root.get(what), param));
////        return sessionFactory.getCurrentSession().createQuery(cq).getSingleResult();
//    }
////
////
    public void update(UserInfo userInfo) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(userInfo);
//        em.getTransaction().begin();
//        em.merge(user);
//        em.getTransaction().commit();
    }
//
//    @SuppressWarnings("unchecked")
//    @Override
//    public List<User> getAll() {
//        Session session = this.sessionFactory.getCurrentSession();
//        return session.createQuery("from User").list();
////        CriteriaQuery<User> criteriaQuery = em.getCriteriaBuilder().createQuery(User.class);
////        @SuppressWarnings("unused")
////        Root<User> root = criteriaQuery.from(User.class);
////        return em.createQuery(criteriaQuery).getResultList();
////        TypedQuery<User> namedQuery = em.createNamedQuery("User.getAll", User.class);
////        return namedQuery.getResultList();
//    }
//
////    public static void storeLoginedUser(HttpSession session, User user) {
////        session.setAttribute("loginedUser", user);
////    }
////
////    public static User getLoginedUser(HttpSession session) {
////        return (User) session.getAttribute("loginedUser");
////    }
}