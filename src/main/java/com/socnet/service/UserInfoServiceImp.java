package com.socnet.service;

import com.socnet.dao.UserDAO;
import com.socnet.dao.UserInfoDAO;
import com.socnet.entities.User;
import com.socnet.entities.UserInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.beans.Transient;
import java.util.List;

@Service
public class UserInfoServiceImp implements UserInfoService{

    private UserInfoDAO userInfoDAO;

    public void setUserInfoDAO(UserInfoDAO userInfoDAO) {
        this.userInfoDAO = userInfoDAO;
    }

    @Override
    @Transactional
    public void add(UserInfo userInfo) {
        this.userInfoDAO.add(userInfo);
    }

    @Override
    @Transactional
    public UserInfo get(long id) {
        UserInfo userInfo = this.userInfoDAO.get(id);
        userInfo.setBlocks();
        return userInfo;
    }

    @Override
    @Transactional
    public void update(UserInfo userInfo){
        this.userInfoDAO.update(userInfo);
    }
}
