package com.socnet.service;

import com.socnet.entities.UserInfo;

public interface UserInfoService {
    void add(UserInfo userInfo);
    UserInfo get(long id);
    void update(UserInfo userInfo);
}
