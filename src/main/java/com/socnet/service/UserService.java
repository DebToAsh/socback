package com.socnet.service;

import com.socnet.entities.User;
import com.socnet.exceptions.UserNotFoundException;

import java.util.List;

public interface UserService {
    void add(User user);
    long addNew(User user);
    List<User> getAll();
    User findBy(String what, String param) throws UserNotFoundException;
    User findByLogin(String param) throws UserNotFoundException;
    void update(User user);
}
