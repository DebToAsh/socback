package com.socnet.service;

import com.socnet.entities.AuthUser;
import com.socnet.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailsServiceImp implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findBy("login", username);

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        List<GrantedAuthority> authorities = new ArrayList<>(1);
        authorities.add(new SimpleGrantedAuthority("ROLE_"+user.getRole().name()));

        return new AuthUser(
                user.getLogin(),
                encoder.encode(user.getPassword()),
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                authorities,
                user.getId()
        );
//        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
//        if (user != null) {
//            builder = org.springframework.security.core.userdetails.User.withUsername(username);
//            builder.password(encoder.encode(user.getPassword()));
//            builder.roles(user.getRole().name());
//        } else {
//            throw new UsernameNotFoundException("User not found.");
//        }
//        return builder.build();
//        Set<GrantedAuthority> roles = new HashSet<>();
//        roles.add(new SimpleGrantedAuthority(user.getRole().name()));
//
//        UserDetails userDetails =
//                new org.springframework.security.core.userdetails.User(user.getLogin(),
//                        encoder.encode(user.getPassword()),
//                        roles);
//        return userDetails;
    }

}
