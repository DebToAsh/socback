package com.socnet.service;

import com.socnet.dao.UserDAO;
import com.socnet.entities.User;
import com.socnet.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    private UserDAO userDAO;

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    @Transactional
    public void add(User user) {
        this.userDAO.add(user);
    }

    @Override
    @Transactional
    public void update(User user) {
        this.userDAO.update(user);
    }

    @Override
    @Transactional
    public long addNew(User user) {
        return this.userDAO.addNew(user);
    }

    @Override
    @Transactional
    public List<User> getAll() {
        return this.userDAO.getAll();
    }

    @Override
    @Transactional
    public User findBy(String what, String param) throws UserNotFoundException {
        User result = userDAO.findBy(what, param);
        if (result == null) {
            throw new UserNotFoundException();
        }
        return result;
    }

    @Override
    @Transactional
    public User findByLogin(String param) {
        return this.userDAO.findBy("login", param);
    }
}
